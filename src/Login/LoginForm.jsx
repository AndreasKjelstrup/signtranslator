import React, { useState } from 'react'

function LoginForm ({ onSuccess }) {

    const [ username, setUsername ] = useState('');

    const onLoginClicked = function() {
        if (username.length === 0 ) {
           return alert('You need to write something as your username');
        }
        localStorage.setItem('st_username', username);
        onSuccess();

    }

    return(
        <section>
        <form action="">
                <label className="InputLabel" htmlFor="username">Username</label>
                <input onChange={(event)=> setUsername(event.target.value.trim())} className="Input" type="text" placeholder="Enter your username..."></input>
        </form>

        <button onClick={onLoginClicked} type="submit" className="Button ButtonPrimary">Login</button>
    </section>
    )
}

export default LoginForm