import React from 'react'
import { useHistory } from 'react-router-dom'
import LoginForm from './LoginForm'

function Login() {


    const history = useHistory();

    function handleSuccessfulLogin() {
        history.push('translations');
    }

    return (
        <div className="Login">
            <div className="Container">
                <header>
                    <h1>Sign translator</h1>
                    <h4>Login to translate some words</h4>
                </header>

                <LoginForm onSuccess={handleSuccessfulLogin} />
            </div>
        </div>

    )
}

export default Login