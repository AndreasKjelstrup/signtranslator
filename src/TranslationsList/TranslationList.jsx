import React, { useContext } from 'react'
import { useHistory } from 'react-router-dom'
import { AppContext } from '../Context/AppContext'
import TranslationAdd from './TranslationAdd'
import TranslationListItem from './TranslationListItem'

function TranslationList() {

    const history = useHistory();
    const { translations } = useContext(AppContext);

    function redirectToProfilePage() {

        history.push('profile');

    }

    return( 
       <main className="TranslationList">
           <h1>Translations</h1>

           <button onClick={redirectToProfilePage} type="submit" className="Button ButtonPrimary">Profile</button>

           <TranslationAdd />

           <ul>
               {
                   translations.map((translation, index) => 
                   <TranslationListItem translation={ translation } key={index} />) 
                }
           </ul>
       </main> 
    )
}

export default TranslationList;