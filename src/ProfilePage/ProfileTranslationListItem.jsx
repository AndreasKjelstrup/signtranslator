import React from 'react'

function ProfileTranslationListItem({ translation }) {
    return(
    <li>{ translation }</li>
    )
}

export default ProfileTranslationListItem