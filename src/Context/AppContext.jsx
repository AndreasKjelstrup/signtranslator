import React, { createContext, useState } from 'react'

export const AppContext = createContext( {
    translations: []
})

export function AppProvider(props) {

    const [ translations, setTranslations ] = useState([]);

    const clearState = () => {
        setTranslations([]);
    }

    const AppState= {
        translations,
        setTranslations,
        clearState
    }

    return (
        <AppContext.Provider value={AppState}>
            {props.children}
        </AppContext.Provider>
    )
}
