import React, { useContext, useState } from 'react'
import { AppContext } from '../Context/AppContext'

export function TranslationAdd() {

    const [ translation, setTranslation ] = useState('');
    const { translations, setTranslations } = useContext( AppContext );

    function onTranslationChange(event) {
        setTranslation(event.target.value.trim());
    }

    function addTranslation() {
        if(translation.length === 0) {
            return alert('Please write something in the input.');
        }
        if(translations.length > 9) {
            translations.pop();
        }
        setTranslations([translation, ...translations]);

    }

    return (
        <div>
            <input type="text" placeholder="Enter something to translate" onChange={ onTranslationChange } />
            <button onClick={addTranslation}>Translate</button>
        </div>
    )
}

export default TranslationAdd;