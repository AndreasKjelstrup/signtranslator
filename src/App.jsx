import React, { lazy, Suspense } from 'react'
import './App.css'
import Login from './Login/Login'
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom'
import { AppProvider } from './Context/AppContext'

const TranslationList = lazy(() => import('./TranslationsList/TranslationList'));
const ProfilePage = lazy(() => import('./ProfilePage/ProfilePage'));

function App() {
  return (
    <BrowserRouter>
      <AppProvider>
        <main className="App">
          <Suspense fallback={<div>Loading</div>}>
              <Switch>
                <Route path="/login" component={ Login } />
                <Route path="/translations" component={ TranslationList } />
                <Route path="/profile" component={ ProfilePage } />
                <Route path="/">
                  <Redirect to="/login" />
                </Route>
              </Switch>
            </Suspense>
          </main>  
      </AppProvider>
    </BrowserRouter>

  );
}

export default App;
