import React from 'react'
import { Signs } from '../Assets/Signs/SignList'

function TranslationListItem({ translation }) {
    return (
        <li>{ translation.split('').map((char, index) => <img alt={ char } key={ index } className="Picture" src={ Signs[char.toLowerCase()] }/>) }</li>
    )
}

export default TranslationListItem