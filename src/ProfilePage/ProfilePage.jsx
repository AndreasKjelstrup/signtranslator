import React, { useContext } from 'react'
import { useHistory } from 'react-router-dom';
import { AppContext } from '../Context/AppContext'
import ProfileTranslationListItem from './ProfileTranslationListItem'

function ProfilePage () {

    const { translations, clearState } = useContext(AppContext);

    const history = useHistory();

    const onLogoutButtonClick = function() {
        let r = window.confirm("Are you sure you wish to log out");

        if (r === true) {
            localStorage.clear();
            history.push('login');
            clearState();
        }
    }

    return( 
       <main className="ProfilePage">
           <h1>My Translations</h1>

           <ul>
               {
                   translations.map((translation, index) => 
                   <ProfileTranslationListItem translation={ translation } key={index} />) 
                }
           </ul>

           <button type="submit" className="Button ButtonDanger" onClick= { onLogoutButtonClick }>Logout</button>
       </main> 
    )
}

export default ProfilePage