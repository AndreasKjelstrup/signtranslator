# Sign translator

This program is a solution to Task03 of Javascript at Experis Academy Denmark

This program is a translator that translates words in to sign language for deaf people.

## Installation
Clone this repository, by running the following git commands: 

```bash
cd existing_folder
git init
git remote add origin git@gitlab.com:AndreasKjelstrup/signtranslator.git
git add .
```

After this is done, run the following commands in a terminal:

```
cd sign-translator
npm start
```

This will start up the server, and then navigate to your chosen location where the app is running at


### Maintainers 
Andreas Kjelstrup

### License
Copyright 2020, Andreas Kjelstrup